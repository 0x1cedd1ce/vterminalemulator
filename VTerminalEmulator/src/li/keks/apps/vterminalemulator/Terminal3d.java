package li.keks.apps.vterminalemulator;

import jackpal.androidterm.emulatorview.TermSession;
import jackpal.androidterm.emulatorview.TermSession.FinishCallback;
import jackpal.androidterm.emulatorview.UpdateCallback;
import rajawali.materials.Material;
import rajawali.materials.methods.DiffuseMethod;
import rajawali.materials.methods.SpecularMethod;
import rajawali.materials.textures.ATexture.TextureException;
import rajawali.materials.textures.Texture;
import rajawali.materials.textures.TextureManager;
import rajawali.math.vector.Vector3.Axis;
import rajawali.primitives.Plane;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

public class Terminal3d extends Plane implements UpdateCallback, FinishCallback {

    public static final int BITMAP_WIDTH = 256;
    public static final int BITMAP_HEIGHT = 256;
    public static final int    NUM_TERMINAL_ROWS = 80;
    public static final int    NUM_TERMINAL_COLS = 80;

    public static ShellTermSession initSession(final Context context) {
        final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        final TermSettings settings = new TermSettings(context.getResources(),
                prefs);
        final ShellTermSession mSession = new ShellTermSession(settings,
                settings.getInitialCommand());
        return mSession;
    }

    private ShellTermSession mSession   = null;
    private Bitmap           mBitmap    = null;
    private boolean          mIsUpdated = false;
    private boolean mIsSelected = false;
    private final String mNameTexture;

    public Texture           mTexture   = null;

    public Terminal3d() {
        this(1f, 1f, 1, 1, Axis.Z, true, false, 1, "text");
    }

    public Terminal3d(final float width, final float height,
            final int segmentsW, final int segmentsH, String nameTexture) {
        this(width, height, segmentsW, segmentsH, Axis.Z, true, false, 1, nameTexture);
    }

    public Terminal3d(final float width, final float height,
            final int segmentsW, final int segmentsH, final Axis upAxis,
            final boolean createTextureCoordinates,
            final boolean createVertexColorBuffer, final int numTextureTiles, String nameTexture) {
        super(width, height, segmentsW, segmentsH, upAxis,
                createTextureCoordinates, createVertexColorBuffer,
                numTextureTiles);
        mNameTexture = nameTexture;
        //setDoubleSided(true);
        //setBackSided(true);
        setTransparent(true);
        setColor(0x00000000);
        mBitmap = Bitmap.createBitmap(segmentsW, segmentsH,
                Bitmap.Config.ARGB_8888);
        mTexture = new Texture(mNameTexture, mBitmap);
        mTexture.setHeight(segmentsH);
        mTexture.setWidth(segmentsW);
    }

    public void initMaterial() {
        final Material mat = new Material();
        try {
            mat.addTexture(mTexture);
        } catch (final TextureException e) {
            e.printStackTrace();
        }
        mat.setDiffuseMethod(new DiffuseMethod.Lambert());
        mat.setSpecularMethod(new SpecularMethod.Phong());
        mat.enableLighting(false);
        setMaterial(mat);
    }

    public boolean onKeyUp(final int keyCode, final KeyEvent event) {
        // mSession.write(event.getCharacters());
        return true;
    }

    @Override
    public synchronized void onSessionFinish(final TermSession session) {
        // TODO Auto-generated method stub

    }

    @Override
    public synchronized void onUpdate() {
        mIsUpdated = true;
    }

    public void setTermSession(final ShellTermSession session) {
        mSession = session;
        mSession.setUpdateCallback(this);
        mSession.setFinishCallback(this);
        mSession.initializeEmulator(NUM_TERMINAL_COLS, NUM_TERMINAL_ROWS);
    }

    public synchronized void updateTexture(final TextureManager textureManager) {
        if (mIsUpdated) {
            final Canvas can = new Canvas(mBitmap);
            can.drawARGB(150, 0, 0, 0);
            final String[] script = mSession.getTranscriptText().split(
                    "\\r?\\n");
            final int start = script.length > NUM_TERMINAL_ROWS ? script.length
                    - NUM_TERMINAL_ROWS : 0;
            final int nOffset = 12;
            final Paint paint = new Paint();
            paint.setColor(Color.GREEN);
            paint.setTextSize(12);
            for (int it = start; it < script.length; it++) {
                can.drawText(script[it], 10, nOffset*(it-start+1), paint);
            }
            mTexture.setBitmap(mBitmap);
            textureManager.replaceTexture(mTexture);
            mIsUpdated = false;
        }
    }
    
    public boolean isSelected() {
        return mIsSelected;
    }
    
    public void setSelected(boolean isSelected) {
        mIsSelected = isSelected;
    }

}
