package li.keks.apps.vterminalemulator;

import java.util.ArrayList;

import rajawali.lights.PointLight;
import rajawali.materials.textures.ATexture.TextureException;
import rajawali.math.Quaternion;
import rajawali.math.vector.Vector3;
import rajawali.math.vector.Vector3.Axis;
import rajawali.vr.RajawaliVRRenderer;
import android.content.Context;
import android.view.KeyEvent;

import com.google.vrtoolkit.cardboard.sensors.MagnetSensor;
import com.google.vrtoolkit.cardboard.sensors.MagnetSensor.OnCardboardTriggerListener;

public class VRRenderer extends RajawaliVRRenderer implements
OnCardboardTriggerListener {
    
    private static int NUM_TERMINALS = 10;

    private MagnetSensor     mMagnetSensor       = null;

    private Terminal3d[]       mTerminals            = null;//new Terminal3d[NUM_TERMINALS];

    private ShellTermSession[] mSessions             = null;//new ShellTermSession[NUM_TERMINALS];

    private boolean          mIsTerminalSelected = false;
    
    private int mIndexSelectedTerminal = -1;

    public VRRenderer(final Context context) {
        super(context);
        mMagnetSensor = new MagnetSensor(context);
        mMagnetSensor.setOnCardboardTriggerListener(this);
        mSessions = new ShellTermSession[NUM_TERMINALS];
        for(int i = 0;i<NUM_TERMINALS;i++) {
            mSessions[i] = Terminal3d.initSession(mContext);
        }
    }

    @Override
    public void initScene() {
        final PointLight pointLight = new PointLight();
        pointLight.setX(0);
        pointLight.setY(1);
        pointLight.setZ(0);
        pointLight.setPower(1);
        getCurrentScene().addLight(pointLight);
        
        final double angle = 2.0 * Math.PI / (double)NUM_TERMINALS;
        mTerminals = new Terminal3d[NUM_TERMINALS];
        for(int i = 0;i<NUM_TERMINALS;i++) {
            mTerminals[i] = new Terminal3d(6, 6, 256, 256, Axis.Z, true, false, 1, "term"+i);
            mTerminals[i].setTermSession(mSessions[i]);
            mTerminals[i].initMaterial();
            final Vector3 pos = new Vector3(0,0,-10);
            mTerminals[i].setPosition(pos.rotateY(angle*i));
            mTerminals[i].setRotY(180-Math.toDegrees(angle*i));
            getCurrentScene().addChild(mTerminals[i]);
        }
        
        try {
            getCurrentScene().setSkybox(R.drawable.posx, R.drawable.negx,
                    R.drawable.posy, R.drawable.negy, R.drawable.posz,
                    R.drawable.negz);
        } catch (final TextureException e) {
            e.printStackTrace();
        }

        super.initScene();
    }

    @Override
    public void onCardboardTrigger() {
        mIsTerminalSelected = !mIsTerminalSelected;
        if(mIsTerminalSelected) {
            mIndexSelectedTerminal = 0;
            final Vector3 newPos = new Vector3(0, 0, -10);
            final Vector3 currentPos = mTerminals[mIndexSelectedTerminal].getPosition();
            Quaternion q = currentPos.getRotationTo(newPos);
            for(Terminal3d terminal : mTerminals) {
                Vector3 pos = terminal.getPosition();
                pos = pos.transform(q);
                terminal.setPosition(pos);
            }
        } else {
            mIndexSelectedTerminal = -1;
        }
    }

    public boolean onKeyUp(final int keyCode, final KeyEvent event) {
        if(mIsTerminalSelected) {
            return mTerminals[mIndexSelectedTerminal].onKeyUp(keyCode, event);
        }
        return true;
    }

    @Override
    public void onRender(final double deltaTime) {
        for(Terminal3d terminal : mTerminals) {
            terminal.updateTexture(mTextureManager);
            if (mIsTerminalSelected) {
                final Vector3 rot = terminal.getPosition();
                mHeadViewMatrix4.rotateVector(rot);
                terminal.setX(rot.x);
                terminal.setZ(rot.z);
                if (rot.z > 0) {
                    terminal.setY(-rot.y);
                } else {
                    terminal.setY(rot.y);
                }
                terminal.setLookAt(0, 0, 0);
            }
        }
        super.onRender(deltaTime);
    }

    /*
     * (non-Javadoc)
     *
     * @see rajawali.renderer.RajawaliRenderer#startRendering()
     */
    @Override
    public void startRendering() {
        mMagnetSensor.start();
        super.startRendering();
    }

    /*
     * (non-Javadoc)
     *
     * @see rajawali.renderer.RajawaliRenderer#stopRendering()
     */
    @Override
    protected boolean stopRendering() {
        mMagnetSensor.stop();
        return super.stopRendering();
    }

}
