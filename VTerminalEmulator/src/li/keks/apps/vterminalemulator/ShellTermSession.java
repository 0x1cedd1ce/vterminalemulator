/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package li.keks.apps.vterminalemulator;

import jackpal.androidterm.emulatorview.ColorScheme;
import jackpal.androidterm.emulatorview.TermSession;
import jackpal.androidterm.emulatorview.UpdateCallback;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * A terminal session, consisting of a TerminalEmulator, a TranscriptScreen, the
 * PID of the process attached to the session, and the I/O streams used to talk
 * to the process.
 */
public class ShellTermSession extends TermSession {
    // ** Set to true to force into 80 x 24 for testing with vttest. */
    private static final boolean VTTEST_MODE                   = false;
    private TermSettings         mSettings;

    private int                  mProcId;
    private FileDescriptor       mTermFd;
    private final Thread         mWatcherThread;

    // A cookie which uniquely identifies this session.
    private String               mHandle;

    private final String         mInitialCommand;

    public static final int      PROCESS_EXIT_FINISHES_SESSION = 0;
    public static final int      PROCESS_EXIT_DISPLAYS_MESSAGE = 1;

    private String               mProcessExitMessage;

    private static final int     PROCESS_EXITED                = 1;

    private final Handler        mMsgHandler                   = new Handler() {
        @Override
        public void handleMessage(
                final Message msg) {
            if (!isRunning()) {
                return;
            }
            if (msg.what == PROCESS_EXITED) {
                onProcessExit((Integer) msg.obj);
            }
        }
    };

    private final UpdateCallback mUTF8ModeNotify               = new UpdateCallback() {
        @Override
        public void onUpdate() {
            Exec.setPtyUTF8Mode(
                    mTermFd,
                    getUTF8Mode());
        }
    };

    public ShellTermSession(final TermSettings settings,
            final String initialCommand) {
        super();

        updatePrefs(settings);

        initializeSession();
        mInitialCommand = initialCommand;

        mWatcherThread = new Thread() {
            @Override
            public void run() {
                Log.i(TermDebug.LOG_TAG, "waiting for: " + mProcId);
                final int result = Exec.waitFor(mProcId);
                Log.i(TermDebug.LOG_TAG, "Subprocess exited: " + result);
                mMsgHandler.sendMessage(mMsgHandler.obtainMessage(
                        PROCESS_EXITED, result));
            }
        };
        mWatcherThread.setName("Process watcher");
    }

    private String checkPath(final String path) {
        final String[] dirs = path.split(":");
        final StringBuilder checkedPath = new StringBuilder(path.length());
        for (final String dirname : dirs) {
            final File dir = new File(dirname);
            if (dir.isDirectory() && FileCompat.canExecute(dir)) {
                checkedPath.append(dirname);
                checkedPath.append(":");
            }
        }
        return checkedPath.substring(0, checkedPath.length() - 1);
    }

    private void createSubprocess(final int[] processId, final String shell,
            final String[] env) {
        ArrayList<String> argList = parse(shell);
        String arg0;
        String[] args;

        try {
            arg0 = argList.get(0);
            final File file = new File(arg0);
            if (!file.exists()) {
                Log.e(TermDebug.LOG_TAG, "Shell " + arg0 + " not found!");
                throw new FileNotFoundException(arg0);
            } else if (!FileCompat.canExecute(file)) {
                Log.e(TermDebug.LOG_TAG, "Shell " + arg0 + " not executable!");
                throw new FileNotFoundException(arg0);
            }
            args = argList.toArray(new String[1]);
        } catch (final Exception e) {
            argList = parse(mSettings.getFailsafeShell());
            arg0 = argList.get(0);
            args = argList.toArray(new String[1]);
        }

        mTermFd = Exec.createSubprocess(arg0, args, env, processId);
    }

    @Override
    public void finish() {
        Exec.hangupProcessGroup(mProcId);
        Exec.close(mTermFd);
        super.finish();
    }

    public String getHandle() {
        return mHandle;
    }

    /**
     * Gets the terminal session's title. Unlike the superclass's getTitle(), if
     * the title is null or an empty string, the provided default title will be
     * returned instead.
     *
     * @param defaultTitle
     *            The default title to use if this session's title is unset or
     *            an empty string.
     */
    public String getTitle(final String defaultTitle) {
        final String title = super.getTitle();
        if (title != null && title.length() > 0) {
            return title;
        } else {
            return defaultTitle;
        }
    }

    @Override
    public void initializeEmulator(int columns, int rows) {
        if (VTTEST_MODE) {
            columns = 80;
            rows = 24;
        }
        super.initializeEmulator(columns, rows);

        Exec.setPtyUTF8Mode(mTermFd, getUTF8Mode());
        setUTF8ModeUpdateCallback(mUTF8ModeNotify);

        mWatcherThread.start();
        sendInitialCommand(mInitialCommand);
    }

    private void initializeSession() {
        final TermSettings settings = mSettings;

        final int[] processId = new int[1];

        String path = System.getenv("PATH");
        if (settings.doPathExtensions()) {
            final String appendPath = settings.getAppendPath();
            if (appendPath != null && appendPath.length() > 0) {
                path = path + ":" + appendPath;
            }

            if (settings.allowPathPrepend()) {
                final String prependPath = settings.getPrependPath();
                if (prependPath != null && prependPath.length() > 0) {
                    path = prependPath + ":" + path;
                }
            }
        }
        if (settings.verifyPath()) {
            path = checkPath(path);
        }
        final String[] env = new String[3];
        env[0] = "TERM=" + settings.getTermType();
        env[1] = "PATH=" + path;
        env[2] = "HOME=" + settings.getHomePath();

        createSubprocess(processId, settings.getShell(), env);
        mProcId = processId[0];

        setTermOut(new FileOutputStream(mTermFd));
        setTermIn(new FileInputStream(mTermFd));
    }

    private void onProcessExit(final int result) {
        if (mSettings.closeWindowOnProcessExit()) {
            finish();
        } else if (mProcessExitMessage != null) {
            try {
                final byte[] msg = ("\r\n[" + mProcessExitMessage + "]")
                        .getBytes("UTF-8");
                appendToEmulator(msg, 0, msg.length);
                notifyUpdate();
            } catch (final UnsupportedEncodingException e) {
                // Never happens
            }
        }
    }

    private ArrayList<String> parse(final String cmd) {
        final int PLAIN = 0;
        final int WHITESPACE = 1;
        final int INQUOTE = 2;
        int state = WHITESPACE;
        final ArrayList<String> result = new ArrayList<String>();
        final int cmdLen = cmd.length();
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < cmdLen; i++) {
            final char c = cmd.charAt(i);
            if (state == PLAIN) {
                if (Character.isWhitespace(c)) {
                    result.add(builder.toString());
                    builder.delete(0, builder.length());
                    state = WHITESPACE;
                } else if (c == '"') {
                    state = INQUOTE;
                } else {
                    builder.append(c);
                }
            } else if (state == WHITESPACE) {
                if (Character.isWhitespace(c)) {
                    // do nothing
                } else if (c == '"') {
                    state = INQUOTE;
                } else {
                    state = PLAIN;
                    builder.append(c);
                }
            } else if (state == INQUOTE) {
                if (c == '\\') {
                    if (i + 1 < cmdLen) {
                        i += 1;
                        builder.append(cmd.charAt(i));
                    }
                } else if (c == '"') {
                    state = PLAIN;
                } else {
                    builder.append(c);
                }
            }
        }
        if (builder.length() > 0) {
            result.add(builder.toString());
        }
        return result;
    }

    private void sendInitialCommand(final String initialCommand) {
        if (initialCommand.length() > 0) {
            write(initialCommand + '\r');
        }
    }

    public void setHandle(final String handle) {
        if (mHandle != null) {
            throw new IllegalStateException("Cannot change handle once set");
        }
        mHandle = handle;
    }

    /*
     * XXX We should really get this ourselves from the resource bundle, but we
     * cannot hold a context
     */
    public void setProcessExitMessage(final String message) {
        mProcessExitMessage = message;
    }

    public void updatePrefs(final TermSettings settings) {
        mSettings = settings;
        setColorScheme(new ColorScheme(settings.getColorScheme()));
        setDefaultUTF8Mode(settings.defaultToUTF8Mode());
    }

    @Override
    public void updateSize(int columns, int rows) {
        if (VTTEST_MODE) {
            columns = 80;
            rows = 24;
        }
        // Inform the attached pty of our new size:
        Exec.setPtyWindowSize(mTermFd, rows, columns, 0, 0);
        super.updateSize(columns, rows);
    }
}
